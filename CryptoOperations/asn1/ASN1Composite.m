//
//  ASN1Composite.m
//  SMIME Reader
//
//  
//  Copyright © 2020 Timothy Perfitt and Joel Rennich. All rights reserved.
//

/*
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.


 */
//

#import "ASN1Composite.h"


@implementation ASN1Composite
@synthesize asn1Type,asn1Category,asn1Class, rangeOfData;

+(ASN1Composite *)asn1Composite{
	
	ASN1Composite *asn1Composite=[[ASN1Composite alloc] init];
	return asn1Composite;
	
}
- (id)init
{
    self = [super init];
    if (self) {
		storageArray=[[NSMutableArray alloc] init];
    }
    return self;
}
- (id)initWithCapacity:(NSUInteger)numItems{
	self = [super init];
    if (self) {
		storageArray=[[NSMutableArray alloc] initWithCapacity:numItems];
    }
    return self;
	
}
-(void)setClass:(int)inClass type:(int)inType category:(int)inCategory {
	asn1Type=inType;
	asn1Category=inCategory;
	asn1Class=inClass;
}

-(id)objectAtIndexPath:(NSString *)inString{
	NSArray *arrayOfIndexes=[inString componentsSeparatedByString:@"."];
	id currentArray=storageArray;
	for (NSString *currIndex in arrayOfIndexes) {
		
		currentArray=[currentArray objectAtIndex:[currIndex intValue]];
		
	}
	return currentArray;
	
}


-(NSUInteger)count{
	return [storageArray count];
	
}
- (id)objectAtIndex:(NSUInteger)index{
	return [storageArray objectAtIndex:index];
}
- (void)insertObject:(id)anObject atIndex:(NSUInteger)index{
	
}
- (void)removeObjectAtIndex:(NSUInteger)index{
	[storageArray removeObjectAtIndex:index];
}
- (void)addObject:(id)anObject{
	[storageArray addObject:anObject];
}
- (void)removeLastObject{
	[storageArray removeLastObject];
}
- (void)replaceObjectAtIndex:(NSUInteger)index withObject:(id)anObject{
	[storageArray replaceObjectAtIndex:index withObject:anObject];
}
-(void)dealloc{
	
}

- (NSString *)descriptionWithLocale:(id)locale indent:(NSUInteger)level{
	return [NSString stringWithFormat:@"Type:%i,Category:%i,Class:%i,Range:%@,%@",
			asn1Type,asn1Category,asn1Class,rangeOfData,[storageArray descriptionWithLocale:locale indent:level]];


}
@end
