//
//  Token.m
//  ExampleTokenExtension
//
//  Created by Timothy Perfitt on 5/8/20.
//  Copyright © 2020 Timothy Perfitt and Joel Rennich. All rights reserved.
//

/*
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.


 */
//

#import "Token.h"
#import "NSData+HexString.h"
@implementation Token

-(Token *)initWithSmartCard:(TKSmartCard *)smartCard driver:(TKSmartCardTokenDriver *)tokenDriver configuration:(TKTokenConfiguration *)configuration{


    if ( self = [super initWithSmartCard:smartCard AID:nil instanceID:configuration.instanceID tokenDriver:tokenDriver]){

        [self.keychainContents fillWithItems:configuration.keychainItems];
    }
    return self;
}

- (TokenSession *)token:(TKToken *)token createSessionWithError:(NSError **)error {
    return [[TokenSession alloc] initWithToken:self];
}

@end
