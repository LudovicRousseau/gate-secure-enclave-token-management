//
//  GatedCommunity.swift
//  TokenContainerApp
//
//  Created by Joel Rennich on 5/9/20.
//  Copyright © 2020 Timothy Perfitt and Joel Rennich. All rights reserved.
//

/*
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.


 */
import Foundation
import CryptoKit
import Cocoa
import CryptoOperations
import LocalAuthentication

extension Data {
    func hexEncodedString() -> String {
        return map { String(format: "%02hhx", $0) }.joined()
    }
}

extension SHA256Digest {
    var string: String {
        get {
            return self.compactMap { String(format: "%02x", $0) }.joined()
        }
    }
}

@objc class GatedCommunity: NSObject {

    private let kCryptoExportImportManagerPublicNumberOfCharactersInALine = 6
    private var encFileName: String? = nil
    
    @objc func genKeyAndCert() {
        let context = LAContext.init()
        context.localizedReason = "Creating a new key in the Secure Enclave"
        
        var flags = SecAccessControlCreateFlags.init()
        flags.formUnion(.privateKeyUsage)
        flags.formUnion(.biometryAny)
        if context.canEvaluatePolicy(.deviceOwnerAuthenticationWithWatch, error: nil) {
            flags.formUnion(.watch)
        }
        guard let access = SecAccessControlCreateWithFlags(nil, kSecAttrAccessibleWhenUnlockedThisDeviceOnly, flags, nil) else { return }
        
        guard let newKey = try? SecureEnclave.P256.Signing.PrivateKey.init(compactRepresentable: true, accessControl: access, authenticationContext: context) else { return }

        CryptoManager.shared().generateCertificate(withPublicKey: newKey.publicKey.x963Representation, commonName: "Secure Enclave", sign: { (data) -> Data in
            return try! newKey.signature(for: data).derRepresentation
        }) { (data) -> Void in
            UserDefaults.init().set(data, forKey: "CertificateData")
            UserDefaults.init().set(newKey.dataRepresentation, forKey: "KeyRep")
        }
    }
    
    func PEMKeyFromDERKey(_ data: Data, header: String?=nil, footer: String?=nil) -> String {

        var resultString: String

        // base64 encode the result
        let base64EncodedString = data.base64EncodedString(options: [])

        // split in lines of 64 characters.
        var currentLine = ""
        resultString = header ?? "-----BEGIN CERTIFICATE-----\n"
        var charCount = 0
        for character in base64EncodedString {
            charCount += 1
            currentLine.append(character)
            if charCount == 64 {
                resultString += currentLine + "\n"
                charCount = 0
                currentLine = ""
            }
        }
        // final line (if any)
        if currentLine.count > 0 { resultString += currentLine + "\n" }
        // final tag
        resultString += footer ?? "-----END CERTIFICATE-----"

        return resultString
    }
    
    @objc func signFromData(objectID: Data, blob: Data) -> Data? {
        
        var signature: Data?
        
        if let privateKey = try? SecureEnclave.P256.Signing.PrivateKey.init(dataRepresentation: objectID) {
            signature = try? privateKey.signature(for: blob).derRepresentation
        }
        return signature
    }
    
    @objc func keyExchangeFromData(objectID: Data, otherKey: Data) -> Data? {
        if let privateKey = try? SecureEnclave.P256.KeyAgreement.PrivateKey.init(dataRepresentation: objectID),
            let publicKey = try? P256.KeyAgreement.PublicKey.init(x963Representation: otherKey) {
            _ = try? privateKey.sharedSecretFromKeyAgreement(with: publicKey)
            return nil
        } else {
            return nil
        }
    }
    
    @objc func encryptFile() {

        if let fileData = getFileToOpen(title: "Select a file to encrypt.", message: "Select a file to encrypt with your Secure Enclave Certificate."),
            let s3cretFile = S3cretFile.init(data: fileData, fileNameData: encFileName?.data(using: .utf8) ?? Data() ),
            let data = s3cretFile.JSONData() {
            saveToFile(data: data, fileName: (encFileName ?? "") + ".s3c")
        }
    }

    @objc func decryptData(fileData:Data){
                let decoder = JSONDecoder.init()
                if var payload = try? decoder.decode(S3cretFile.self, from: fileData),
                    let decryptedFile = payload.decrypt(),
                    let fileName = String.init(data: payload.fileNameDecrypted ?? Data(), encoding: .utf8) {
                    RunLoop.main.perform {
                        self.saveToFile(data: decryptedFile,fileName: fileName)
                    }
        }
    }
    
    @objc func decryptFile() {

        if let fileData = self.getFileToOpen(title: "Select an encrypted file", message: "Select a file to decrypt with your Secure Enclave.") {

            decryptData(fileData: fileData)

        }
    }
    
    @objc func setPin() {
        let alert = NSAlert.init()
        alert.messageText = "Please enter a PIN"
        let localPassword = NSSecureTextField(frame: CGRect(x: 0, y: 0, width: 200, height: 24))
        alert.accessoryView = localPassword
        localPassword.becomeFirstResponder()
        alert.beginSheetModal(for: NSApp.mainWindow!, completionHandler: { response in
            if !localPassword.stringValue.isEmpty {
                if let pinData = localPassword.stringValue.data(using: .utf8) {
                    let pinHash = SHA256.hash(data: pinData).string
                    UserDefaults.init().set(pinHash, forKey: "PINHash")
                }
            } else {
                UserDefaults.init().set(nil, forKey: "PINHash")
            }
        })
    }
    
    @objc func validatePIN(pin: String, pinHash: String) -> Bool {
        
        if pinHash == "NONE" {
            return true
        }
        
        if let pinData = pin.data(using: .utf8) {
            if (pinHash == SHA256.hash(data: pinData).string) {
                return true
            }
        }
        return false
    }
    
    private func getCertFromPrefs() -> SecCertificate? {
        if let certData = UserDefaults.init().data(forKey: "CertificateData") {
            return SecCertificateCreateWithData(nil, certData as CFData)
        } else {
            return nil
        }
    }
    
    private func getFileToOpen(title: String, message: String) -> Data? {
        let panel = NSOpenPanel.init()
        panel.prompt = "Open"
        panel.worksWhenModal = true
        panel.allowsMultipleSelection = false
        panel.canChooseDirectories = false
        panel.resolvesAliases = true
        panel.title = title
        panel.message = message
        panel.runModal()
        let fileURLs = panel.urls
        if let fileURL = fileURLs.first {
            encFileName = fileURL.lastPathComponent
            return try? Data.init(contentsOf: fileURL, options: .init())
        } else {
            return nil
        }
    }
    
    private func saveToFile(data: Data, fileName: String?) {
        let panel = NSSavePanel()
        panel.nameFieldStringValue = fileName ?? ""
        panel.begin(completionHandler: { response in
            if response == .OK {
                if let url = panel.url {
                    try! data.write(to: url)
                }
            }
        })
    }
}
