
[![](http://img.youtube.com/vi/Som9fIj7ldA/0.jpg)](http://www.youtube.com/watch?v=Som9fIj7ldA "Gate: Secure Enclave Token Management")

# Gate README

A 2020 Pandemic Project by Timothy Perfitt and Joel Rennich
## Overview
Gate is sample macOS app that contains a CryptoTokenKit (CTK) extension and demonstrates some new ways to work with tokens in macOS:

1. Insert and remove X.509 certificates into the keychain API without a physical smartcard insertion event. Applications can insert certificates into the keychain that are used for cryptographic operations with an embedded CryptoTokenKit extension.

2. Associate a certificate with a ECC private key created in the Secure Enclave on T2-enabled Macs.

3. Authenticate with built-in authentication (Login Window, Screen Saver, System Preferences locks, sudo, ssh, web) using an identity in the Secure Enclave.

## Inserting Identities with TKTokenDriverConfiguration
In macOS 10.15, the [TKTokenDriverConfiguration] (https://developer.apple.com/documentation/cryptotokenkit/tktokendriverconfiguration) class was introduced for adding in identities into the keychain API. A TKTokenDriverConfiguration object is created by the containing app, an identity can be created and inserted into the configuration, and that configuration is added to the token driver configuration. This configuration is persistent. It remains loaded after the app is quit as well as across reboots.

An example of loading this configuration is show below:

~~~~
-(void)loadConfig:(id)sender{

    NSDictionary *driverConfigDict=[TKTokenDriverConfiguration driverConfigurations];
    TKTokenDriverConfiguration* driverConfig=driverConfigDict[@"com.twocanoes.gate.TokenExtension"];
    NSDictionary *tokenConfigDict=[driverConfig tokenConfigurations];
    TKTokenConfiguration *tokenConfig=tokenConfigDict[TOKENID];
    
    tokenConfig=[driverConfig addTokenConfigurationForTokenInstanceID:TOKENID];
    
    NSMutableArray *items=[NSMutableArray array];
    //... Add certificate and reference to private key to array ...
    tokenConfig.keychainItems=[NSArray arrayWithArray:items];`
}
~~~~

The TOKENID is a unique identifier for the configuration and can be any unique NSString. A driver can have multiple configurations and can load and unload them as needed.

This configuration mirrors the process the driver would normally preform when it is activated on an insertion event.

## Classes
A custom CryptoTokenKit extension consists of subclasses of 3 classes:

* **TKTokenDriver**: Subclass to provide TKTokens for cryptographic operations. This is the entry point for loading an extension and the subclass name is defined in the CTK extension Info.plist. 
* **TKToken**: Subclass to provide TKTokenSession that does the cryptographic operations. 
* **TKTokenSession**: Subclass to provide crypto operations such as signing and encrypting associated with the TKToken subclass.

The info.plist for the CTK extension has the driver class defined with the com.apple.ctk.driver-class key and the name of the driver class as an NSString. Normally this file would also contain the AID, or application ID, of the particular flavor of smart card, e.g. a PIV compliant card. However, since there is no card backing this token, there's no need for any AID at all.

The driver class defined in the extension plist is the name of the subclass of TKTokenDriver (or its subclass TKSmartcardDriver) and must conform to the TKTokenDriverDelegate delegate. When the driver is loaded, a message is sent to then tokenDriver:tokenForConfiguration:error: method to initialize the driver and create the TKToken used in a TKTokenSession.

The configuration passed to the tokenDriver:tokenForConfiguration:error: is the  configuration defined in the container app for the extension. The certificates then become available to the application that use the keychain API. The tokenDriver:tokenForConfiguration:error: returns the TKToken subclass that implements the TKTokenDelegate, specifically:

~~~
- (TKTokenSession *)token:(TKToken *)token createSessionWithError:(NSError **)error;`
~~~

This methods is called when a cryptographic operation is requested to create the session. The TKTokenSession subclass that is returned determines what cryptographic operations are supported and performs the crypto operations. This subclass must conforms to the TKTokenSessionDelegate delegate and should implement the crypto operations defined in the delegate:

~~~
- (BOOL)tokenSession:(TKTokenSession *)session supportsOperation:(TKTokenOperation)operation usingKey:(TKTokenObjectID)keyObjectID algorithm:(TKTokenKeyAlgorithm *)algorithm {

- (NSData *)tokenSession:(TKSmartCardTokenSession *)session signData:(NSData *)dataToSign usingKey:(TKTokenObjectID)keyObjectID algorithm:(TKTokenKeyAlgorithm *)algorithm error:(NSError **)error;

- (NSData *)tokenSession:(TKTokenSession *)session decryptData:(NSData *)ciphertext usingKey:(TKTokenObjectID)keyObjectID algorithm:(TKTokenKeyAlgorithm *)algorithm error:(NSError **)error;

- (NSData *)tokenSession:(TKTokenSession *)session performKeyExchangeWithPublicKey:(NSData *)otherPartyPublicKeyData usingKey:(TKTokenObjectID)objectID algorithm:(TKTokenKeyAlgorithm *)algorithm parameters:(TKTokenKeyExchangeParameters *)parameters error:(NSError **)error;

-(NSData *)signData:(NSData *)inData withPrivateKey:(SecKeyRef)privateKeyRef;
~~~

Gate uses a custom CTK driver not just for signing operations, but for system authentication as well. In order for the containing app to insert a smartcard into the system from a CTK driver, the TKToken must be a a subclass of a TKSmartCardToken and the delegate tokenDriver:tokenForConfiguration:error: method returns a TKSmartCard instead of a TKToken:

~~~
- (TKToken *)tokenDriver:(TKSmartCardTokenDriver *)driver tokenForConfiguration:(TKTokenConfiguration *)configuration error:(NSError **)error{

    TKSmartCard *smartCard=[[TKSmartCard alloc] init];

    return [[Token alloc] initWithSmartCard:smartCard driver:driver configuration:configuration];
}
~~~
The TKTokenDriver subclass must conform to the TKSmartCardTokenDriverDelegate and return the TKSmartCard subclass:

~~~
- (TKToken *)tokenDriver:(TKSmartCardTokenDriver *)driver tokenForConfiguration:(TKTokenConfiguration *)configuration error:(NSError **)error;
~~~
The subclass must also implement tokenDriver:createTokenForSmartCard:AID:error: but it can return nil since it will never be called.

A driver can require a PIN to be validated before allowing the operation to proceed. The mechanics of this can seem a bit weird at first. The macOS UI will always prompt the user for a PIN, however, the PIN will only be passed on to the driver if the driver throws a `TKErrorCodeAuthenticationNeeded` error.

Once thrown, the driver will then be called and expected to provide a completion block to handle validating the PIN.

~~~
- (TKTokenAuthOperation *)tokenSession:(TKTokenSession *)session beginAuthForOperation:(TKTokenOperation)operation constraint:(TKTokenOperationConstraint)constraint error:(NSError * _Nullable __autoreleasing *)error
~~~

To the user, at least in most cases, they have no idea if the Token will require a PIN or not, as the user expereince is the same. The exception is for apps that are not really CTK-aware and just think they are getting an identity from the Keychain. For example, not throwing this error will cause Safari and Mail.app to not require any PIN validation for the user when using the identity in the application.

## Potential Uses
The ability to insert X.509 certificates into the macOS keychain opens up a large number of possibility for apps that want to use certificate-based authentication. Potential uses include:

* Hardware Security Module (HSM): A code signing certificate is discovered from a network HSM and configured for use by the macOS keychain. Signing operations are sent over the network by the CTK extension.

* Secure Enclave Authentication: The secure enclave is a hardware cryptographic module that does not permit private keys to be exported. If a certificate is trusted by an external system, it is known to be associated with the specific Mac and not a different device.

* Additional Factor in Authentication: During authentication, it would be possible to limit authentication to a specific device. Certificate-based authentication from the Secure Enclave makes this possible.

* Limit Services to a Specific Device: If services such as VPN or Wireless (802.1x) need to be limited to a specific Mac, the service can be associate with a certificate whose private key is in the secure enclave.

Note: In this implemention we made the conscious decision to require a biometric, including Apple Watch, operation before the private key can be used. This will complicate using Gate as a CTK token at the loginwindow or the screen saver of a macOS system. It appears that the UI to request the biometric operation can't be shown in those situations.

Removing the biometric requirement will allow Gate to be fully functional for every function we could find. However, removing the biometric requirement also seems to allow any application that knows the reference to the key in the Secure Encalave to use it as well.

## File Encryption
During the creation of this project, we were looking for practical consumer reasons for doing this. Asking the age old tech question "what would grandma use this for?" To that end, we took a quick detour into using Ephemeral ECDH encryption via the ChaChaPoly algorithm and EC keys. This is a really good example of what can be done with the Secure Enclave.

Using Gate's file encryption you can encrypt files that can only be decrytped by accessing the Secure Enclave on the device where the file was originally encrypted. As mentioned ealier we also decided to require biometrics before being able to use the private key in the enclave giving the user a very simple and secure way to encrypt files without having to have a password to remember.

## License
Gate is license under GPLv3. [Reach out](https://twocanoes.com/contact/) if you need assistance building custom CTK solutions or for licensing for your project.
 
